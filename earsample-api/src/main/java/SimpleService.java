import javax.ejb.Local;
import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: norman
 * Date: 02.12.13
 * Time: 19:39
 * To change this template use File | Settings | File Templates.
 */
@Local
public interface SimpleService extends Serializable {
    String sayHello();
}
