import javax.ejb.Local;
import javax.ejb.Stateless;

/**
 * Created with IntelliJ IDEA.
 * User: norman
 * Date: 02.12.13
 * Time: 19:43
 * To change this template use File | Settings | File Templates.
 */
@Local(SimpleService.class)
@Stateless
public class SimpleServiceImpl implements SimpleService{
    @Override
    public String sayHello() {
        return "Hello you";
    }
}
